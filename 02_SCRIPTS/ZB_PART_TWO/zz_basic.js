function fn()
{
    // "On document ready" commands:
    console.log(document.readyState);

    var simpleFn = () => "Simple Function";
    console.log(simpleFn());


    var simpleFn1 = () => {
        let value = "Simple Function";
        return value;
    } ;//for multiple statement wrap with { }

    console.log(simpleFn1());



    var simpleFn2 = () => { //function scope
        if(true) {
            let a = 1;
            var b = 2;
            console.log(a);
            console.log(b);
        } //if block scope
        console.log("var"+b) ;//function scope;
        //console.log("let"+a); //function scope
    };

    console.log(simpleFn2());
 

}






if(document.readyState != 'loading')
{
    fn();

    /*
    https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState
    */
}
else
{
    document.addEventListener('DOMContentLoaded', fn);
}



