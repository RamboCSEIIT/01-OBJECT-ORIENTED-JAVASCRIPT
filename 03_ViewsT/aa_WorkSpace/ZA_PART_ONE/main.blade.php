@extends('ZA_PART_ONE.base')

@section('title')
    ZA_PART_ONE
@endsection

@section('laravelBlock')
   {{--   @include('ZA_PART_ONE.aa_include.zd_laravel_files')--}}
@endsection

@section('cssBlock')

@endsection

@section('content')
@include('ZA_PART_ONE.aa_include.zc_navbar')
<main>
@include('zz_body.ZA_PART_ONE.body')
</main>
@include('ZA_PART_ONE.aa_include.ze_footer')
@endsection


@section('bottomJS')



@endsection


 
  