<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Admin;
use App\User;
use App\Role_admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $number_of_admins    = 2;
        $number_of_users     = 4;

        $role_1 = new Role;
        $role_1->name = "admin";
        $role_1->save();
        $role_2= new Role;
        $role_2->name = "editor";
        $role_2->save();


        $factory=factory(Admin::class  ,$number_of_admins);
        $factory->create();



       // $factory=factory(User::class  ,$number_of_users);
       // $factory->create();



        $role_admin_1 = new App\Role_admin();
        $role_admin_1->role_id  =  Role::first()->id;
        $role_admin_1->admin_id =  Admin::first()->id;
        $role_admin_1->save();


        $role_admin_2 = new App\Role_admin();
        $role_admin_2->role_id  =  Role::all()->last()->id;
        $role_admin_2->admin_id =  Admin::all()->last()->id;
        $role_admin_2->save();




















    }
}
