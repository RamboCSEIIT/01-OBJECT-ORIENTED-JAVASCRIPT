<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class EditorController extends Controller
{
    //

    public function index()
    {
        return view('admin.editor');
    }


    public function __construct()
    {
        $this->middleware('auth:admin');
        //Only editor you just add here with out excepy
        $this->middleware('editor',['except'=>'test']);
    }

    public function test()
    {
        return view('admin.test');
    }
}
