(function() {
    'use strict';
    //the rest of the function
}());
/*
function supports_html5_storage(){
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch(e) {
        return false;
    }
}
function supports_webworker() {
    if (typeof(Worker) !== "undefined") {
        return true;
    }
    else{
        return false;
    }
}
*/
function WebWorker_Start() {
    /*
         if('serviceWorker' in navigator)
         {
             navigator.serviceWorker
                 .register('/aa_test_worker.js')
                 .then(function() { console.log("Service Worker Registered"); })
                 .catch(function (err) {
                     console.error(err); // the Service Worker didn't install correctly
                 });;
         }
    */
}
/*
var x = [];
function grow() {
    for (var i = 0; i < 10000; i++) {
        document.body.appendChild(document.createElement('div'));
    }
    x.push(new Array(1000000).join('x'));
}
document.getElementById('TestID').addEventListener('click', grow);
*/
/*
var detachedNodes;
function create() {
    var ul = document.createElement('ul');
    for (var i = 0; i < 10; i++) {
        var li = document.createElement('li');
        ul.appendChild(li);
    }
    detachedNodes = ul;
}
document.getElementById('TestID').addEventListener('click', create);
*/
function fn() {
    // "On document ready" commands:
    console.log(document.readyState);
    var simpleFn = () => "Simple Function";
    console.log(simpleFn());
    var simpleFn1 = () => {
        let value = "Simple Function";
        return value;
    }; //for multiple statement wrap with { }
    console.log(simpleFn1());
    var simpleFn2 = () => { //function scope
        if (true) {
            let a = 1;
            var b = 2;
            console.log(a);
            console.log(b);
        } //if block scope
        console.log("var" + b); //function scope;
        //console.log("let"+a); //function scope
    };
    console.log(simpleFn2());
}
if (document.readyState != 'loading') {
    fn();
    /*
    https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState
    */
} else {
    document.addEventListener('DOMContentLoaded', fn);
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX2xlZ2FjeV9jaGVjay5qcyIsImFiX2NhY2hlX3dvcmtlci5qcyIsImFjX2NsYXNzLmpzIiwienpfYmFzaWMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNsQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJ6Yl9wYXJ0X3R3by5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuKGZ1bmN0aW9uICgpIHtcbiAgICAndXNlIHN0cmljdCc7XG4gICAgLy90aGUgcmVzdCBvZiB0aGUgZnVuY3Rpb25cbn0oKSk7XG5cbi8qXG5mdW5jdGlvbiBzdXBwb3J0c19odG1sNV9zdG9yYWdlKCl7XG4gICAgdHJ5IHtcbiAgICAgICAgcmV0dXJuICdsb2NhbFN0b3JhZ2UnIGluIHdpbmRvdyAmJiB3aW5kb3dbJ2xvY2FsU3RvcmFnZSddICE9PSBudWxsO1xuICAgIH0gY2F0Y2goZSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxufVxuXG5cbmZ1bmN0aW9uIHN1cHBvcnRzX3dlYndvcmtlcigpIHtcbiAgICBpZiAodHlwZW9mKFdvcmtlcikgIT09IFwidW5kZWZpbmVkXCIpIHtcblxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgZWxzZXtcblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxufVxuXG4qLyIsIlxuIGZ1bmN0aW9uIFdlYldvcmtlcl9TdGFydCgpIHtcblxuXG4vKlxuXG4gICAgIGlmKCdzZXJ2aWNlV29ya2VyJyBpbiBuYXZpZ2F0b3IpXG4gICAgIHtcbiAgICAgICAgIG5hdmlnYXRvci5zZXJ2aWNlV29ya2VyXG4gICAgICAgICAgICAgLnJlZ2lzdGVyKCcvYWFfdGVzdF93b3JrZXIuanMnKVxuICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKCkgeyBjb25zb2xlLmxvZyhcIlNlcnZpY2UgV29ya2VyIFJlZ2lzdGVyZWRcIik7IH0pXG4gICAgICAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpOyAvLyB0aGUgU2VydmljZSBXb3JrZXIgZGlkbid0IGluc3RhbGwgY29ycmVjdGx5XG4gICAgICAgICAgICAgfSk7O1xuICAgICB9XG4qL1xufVxuXG4iLCIvKlxuXG52YXIgeCA9IFtdO1xuXG5mdW5jdGlvbiBncm93KCkge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgMTAwMDA7IGkrKykge1xuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpKTtcbiAgICB9XG4gICAgeC5wdXNoKG5ldyBBcnJheSgxMDAwMDAwKS5qb2luKCd4JykpO1xufVxuXG5kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnVGVzdElEJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBncm93KTtcbiovXG4vKlxudmFyIGRldGFjaGVkTm9kZXM7XG5cbmZ1bmN0aW9uIGNyZWF0ZSgpIHtcbiAgICB2YXIgdWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd1bCcpO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgMTA7IGkrKykge1xuICAgICAgICB2YXIgbGkgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdsaScpO1xuICAgICAgICB1bC5hcHBlbmRDaGlsZChsaSk7XG4gICAgfVxuICAgIGRldGFjaGVkTm9kZXMgPSB1bDtcbn1cblxuZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ1Rlc3RJRCcpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY3JlYXRlKTtcbiovXG5cbiIsImZ1bmN0aW9uIGZuKClcbntcbiAgICAvLyBcIk9uIGRvY3VtZW50IHJlYWR5XCIgY29tbWFuZHM6XG4gICAgY29uc29sZS5sb2coZG9jdW1lbnQucmVhZHlTdGF0ZSk7XG5cbiAgICB2YXIgc2ltcGxlRm4gPSAoKSA9PiBcIlNpbXBsZSBGdW5jdGlvblwiO1xuICAgIGNvbnNvbGUubG9nKHNpbXBsZUZuKCkpO1xuXG5cbiAgICB2YXIgc2ltcGxlRm4xID0gKCkgPT4ge1xuICAgICAgICBsZXQgdmFsdWUgPSBcIlNpbXBsZSBGdW5jdGlvblwiO1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgfSA7Ly9mb3IgbXVsdGlwbGUgc3RhdGVtZW50IHdyYXAgd2l0aCB7IH1cblxuICAgIGNvbnNvbGUubG9nKHNpbXBsZUZuMSgpKTtcblxuXG5cbiAgICB2YXIgc2ltcGxlRm4yID0gKCkgPT4geyAvL2Z1bmN0aW9uIHNjb3BlXG4gICAgICAgIGlmKHRydWUpIHtcbiAgICAgICAgICAgIGxldCBhID0gMTtcbiAgICAgICAgICAgIHZhciBiID0gMjtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGEpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coYik7XG4gICAgICAgIH0gLy9pZiBibG9jayBzY29wZVxuICAgICAgICBjb25zb2xlLmxvZyhcInZhclwiK2IpIDsvL2Z1bmN0aW9uIHNjb3BlO1xuICAgICAgICAvL2NvbnNvbGUubG9nKFwibGV0XCIrYSk7IC8vZnVuY3Rpb24gc2NvcGVcbiAgICB9O1xuXG4gICAgY29uc29sZS5sb2coc2ltcGxlRm4yKCkpO1xuIFxuXG59XG5cblxuXG5cblxuXG5pZihkb2N1bWVudC5yZWFkeVN0YXRlICE9ICdsb2FkaW5nJylcbntcbiAgICBmbigpO1xuXG4gICAgLypcbiAgICBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9BUEkvRG9jdW1lbnQvcmVhZHlTdGF0ZVxuICAgICovXG59XG5lbHNlXG57XG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZuKTtcbn1cblxuXG5cbiJdfQ==
