var cacheWhitelist = ['pages-cache-1'];
//const NewcacheName = 'v1';

const cacheAssets = [
    '/02_SCRIPTS/jquery-0.js',
    '/02_SCRIPTS/jquery-1.js',
    '/02_SCRIPTS/jquery-2.js',
    '/flexbox2'

];

self.addEventListener('install', function(event) {

    event.waitUntil(
        caches.open(cacheWhitelist[0])
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(cacheAssets);
            })
            .then(function( ) {
                self.skipWaiting()
            })

    );


    console.log('Service Worker: install');
});
self.addEventListener('activate', function(event) {

/*
So, the waitUntil method is used to tell the browser not to terminate the service worker until the promise passed
to waitUntil is either resolved or rejected.

*/

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );

/*
    // Remove unwanted caches
    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cache)  {
                    if (cache !== NewcacheName) {
                        console.log('Service Worker: Clearing Old Cache');
                        return caches.delete(cache);
                    }
                })
            );
        })
    );
*/
    console.log('Service Worker: activate');
});
/*
self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                // Cache hit - return response
                if (response) {
                    return response;
                }

                // IMPORTANT: Clone the request. A request is a stream and
                // can only be consumed once. Since we are consuming this
                // once by cache and once by the browser for fetch, we need
                // to clone the response.
                var fetchRequest = event.request.clone();

                return fetch(fetchRequest).then(
                    function(response) {
                        // Check if we received a valid response
                        if(!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }

                        // IMPORTANT: Clone the response. A response is a stream
                        // and because we want the browser to consume the response
                        // as well as the cache consuming the response, we need
                        // to clone it so we have two streams.
                        var responseToCache = response.clone();

                        caches.open(cacheWhitelist[0])
                            .then(function(cache) {
                                cache.put(event.request, responseToCache);
                            });

                        return response;
                    }
                );
            })
    );
});
*/

// Don't do this!
self.addEventListener('fetch', event => {
    event.respondWith(fetch(event.request));
});