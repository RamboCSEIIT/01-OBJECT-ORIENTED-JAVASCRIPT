
<div class="navigation">
    <input class="navigation__checkbox" id="navi-toggle" type="checkbox">

    <label class="navigation__button" for="navi-toggle">
         <span class="navigation__icon">&nbsp;</span>
    </label>

    <div class="navigation__background">&nbsp;</div>

    <nav class="navigation__nav">
        <ul class="navigation__list">
            <li class="navigation__item"><a class="navigation__link" href="#"><span>01</span>About Company</a></li>
            <li class="navigation__item"><a class="navigation__link" href="#"><span>02</span>Your benfits</a></li>
            <li class="navigation__item"><a class="navigation__link" href="#"><span>03</span>Popular tours</a></li>
            <li class="navigation__item"><a class="navigation__link" href="#"><span>04</span>Stories</a></li>
            <li class="navigation__item"><a class="navigation__link" href="#"><span>05</span>Book now</a></li>
        </ul>
    </nav>
</div>

