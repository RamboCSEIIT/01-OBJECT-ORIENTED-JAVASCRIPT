(function () {
    'use strict';
    //the rest of the function
}());
/*
function supports_html5_storage(){
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch(e) {
        return false;
    }
}
function supports_webworker() {
    if (typeof(Worker) !== "undefined") {
        return true;
    }
    else{
        return false;
    }
}
*/
 function WebWorker_Start() {
/*
     if('serviceWorker' in navigator)
     {
         navigator.serviceWorker
             .register('/aa_test_worker.js')
             .then(function() { console.log("Service Worker Registered"); })
             .catch(function (err) {
                 console.error(err); // the Service Worker didn't install correctly
             });;
     }
*/
}
/*
var x = [];
function grow() {
    for (var i = 0; i < 10000; i++) {
        document.body.appendChild(document.createElement('div'));
    }
    x.push(new Array(1000000).join('x'));
}
document.getElementById('TestID').addEventListener('click', grow);
*/
/*
var detachedNodes;
function create() {
    var ul = document.createElement('ul');
    for (var i = 0; i < 10; i++) {
        var li = document.createElement('li');
        ul.appendChild(li);
    }
    detachedNodes = ul;
}
document.getElementById('TestID').addEventListener('click', create);
*/
function fn()
{
    // "On document ready" commands:
    console.log(document.readyState);
    var simpleFn = () => "Simple Function";
    console.log(simpleFn());
    var simpleFn1 = () => {
        let value = "Simple Function";
        return value;
    } ;//for multiple statement wrap with { }
    console.log(simpleFn1());
    var simpleFn2 = () => { //function scope
        if(true) {
            let a = 1;
            var b = 2;
            console.log(a);
            console.log(b);
        } //if block scope
        console.log("var"+b) ;//function scope;
        //console.log("let"+a); //function scope
    };
    console.log(simpleFn2());
}
if(document.readyState != 'loading')
{
    fn();
    /*
    https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState
    */
}
else
{
    document.addEventListener('DOMContentLoaded', fn);
}
